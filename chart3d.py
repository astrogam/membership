import matplotlib.pyplot as plot        
import csv
import matplotlib.markers as markers
import matplotlib.ticker as mticker
from matplotlib.patches import Ellipse

args = {}
args['ra'] = '304.53'
args['dec'] = '40.73'
args['radius'] = '800'
ra = 304.53
dec = 40.73
radius = 800 / 3600
dataset_a = []
dataset_b = []

f = 'group_A.csv'
#f = 'potential_OC_2.csv'
with open(f,'r') as fp:
    for sample in csv.DictReader(fp):
        dataset_a.append(sample)
f = 'group_B.csv'
#f = 'OC_members/Collinder_419.csv'
with open(f,'r') as fp:
    for sample in csv.DictReader(fp):
        dataset_b.append(sample)



# Plot samples before centering in position (dataset_corrected)
if True:
        #fig, axes = plot.subplots(nrows=1, ncols=2, figsize=(15,20))
        #ax = axes.flatten()[0]
        fig = plot.figure()
        ax = fig.add_subplot(projection='3d')
        #ax.set_aspect('equal')
        title = 'Declination vs Rect ascension \n'
        title += r'Center in $\alpha$ = ' + args['ra'] + " deg, $\delta$ = " + args['dec'] + " deg, r = " + args['radius'] + ' arcsec'
        ax.set_title(title)
        ax.set_ylabel(r'$\delta$ (deg)')
        ax.set_xlabel(r'$\alpha$ (deg)')
        ax.set_zlabel(r'$\omega$ (deg)')
        ax.yaxis.set_minor_locator(mticker.AutoMinorLocator())
        ax.xaxis.set_minor_locator(mticker.AutoMinorLocator())
        x = [float(s['ra']) for s in dataset_a]
        y = [float(s['dec']) for s in dataset_a]
        z = [float(s['parallax']) for s in dataset_a]
        #e = Ellipse((ra, dec), 2 * radius, 2 * radius, color='red', alpha=0.2)
        x_in = [float(s['ra']) for s in dataset_b]
        y_in = [float(s['dec']) for s in dataset_b]
        z_in = [float(s['parallax']) for s in dataset_b]

        #ax.add_artist(e)
        ax.scatter(x, y, z, c='brown', s=6)
        ax.scatter(x_in, y_in, z_in, marker='X', c='blue', s=6)
        plot.show()

