import pickle
import numpy as np
import os
import argparse
import sys
import csv
from pprint import pprint
import math
import matplotlib.markers as markers
import matplotlib.ticker as mticker
import matplotlib.pyplot as plot
from matplotlib.patches import Ellipse
from sklearn.neighbors import KernelDensity
import astropy.units as u
from astropy.coordinates import SkyCoord
from astroquery.gaia import Gaia
from astropy.io import ascii

U0_table_file = "DR2_RUWE_V1/table_u0_g_col.txt"
V1_table_file = "DR2_spatialCov_V1/spatialCovarianceQso.txt"
five_param_astrometry = ['source_id','ra','ra_error','dec','dec_error','parallax','parallax_error', \
        'pmra','pmra_error','pmdec','pmdec_error','astrometric_n_good_obs_al','astrometric_chi2_al', \
        'phot_g_mean_mag','phot_bp_mean_mag','phot_rp_mean_mag','visibility_periods_used', \
        'phot_bp_rp_excess_factor','bp_rp','bp_g','g_rp']
my_cluster_file = 'clusters.csv'

def load_U0_table_file():
    U0 = []
    # Headers g_mag, bp_rp, u0
    with open(U0_table_file, 'r') as fp:
        for sample in csv.DictReader(fp):
            row = [float(sample['g_mag']), float(sample['bp_rp']), float(sample['u0'])]
            U0.append(row)
    U0 = np.array(U0)
    return U0

def find_nearest(array, value):
    idx = (np.abs(array - value)).argmin()
    return array[idx]

def load_V1_table_file():
    V1 = []
    # Headers theta, V_plx, V_mu
    with open(V1_table_file, 'r') as fp:
        for sample in csv.DictReader(fp):
            row = [float(sample['theta']), float(sample['V_plx']), float(sample['V_mu'])]
            V1.append(row)
    V1 = np.array(V1)
    return V1

def get_spatial_covariance(theta, V1):
    # Returns spatial covariance of plx_1, plx_2 in mas^2 (in V1 is microas^2, so *1/1000000)
    # theta in degrees
    theta = abs(theta * 180 / math.pi)
    # Find nearest
    idx = (np.abs(V1[:,0] - theta)).argmin()
    return V1[idx, 1] / 1000000

def get_pm_spatial_covariance(theta, V1):
    # Returns spatial covariance of pm_1, pm_2 in mas^2 (in V1 is microas^2, so *1/1000000)
    # theta in degrees
    theta = abs(theta * 180 / math.pi)
    # Find nearest
    idx = (np.abs(V1[:,0] - theta)).argmin()
    return V1[idx, 2] / 1000000

def buildParser():
    # construct the argument parser and parse the arguments
    ap = argparse.ArgumentParser()
    ap.add_argument("-f", "--file", required=False,  
        help="Path to CSV file with Gaia sources \
              IMPORTANT: Export the following columns: " + str(five_param_astrometry) + ". \n If the file is not provided, \
              will try to download data from Gaia API (ra/dec and radius of field size)")
    ap.add_argument("-of", "--output-file",  required=False,  
        help="If the file (-f) is not provided, CSV output file with sources extracted from Gaia archive. Default: Gaia_sources.csv")
    ap.add_argument("-ocf", "--output-cluster-file",  required=False,  
        help="CSV output file with all members of star open cluster. Default: OC_members.csv")
    ap.add_argument("-n", "--name",  required=False,  
        help="Cluster name. If provided, we add or edit it in clusters file: " + my_cluster_file)
    ap.add_argument("-ra", "--ra",  required=True,  
        help="Open cluster rect ascension in degrees")
    ap.add_argument("-d", "--dec", required=True,  
        help="Open cluster declination in degrees")
    ap.add_argument("-r", "--radius", required=True,  
        help="Open cluster radius in seconds of arc (arcs).")
    ap.add_argument("-fs", "--field-size", required=False,  
        help="Open cluster field size in seconds of arc (arcs). A square of that size, eg. 3600 x 3600 arcs (1 degree^2)")
    ap.add_argument("-pmra", "--pmra",  required=False,
        help="Open cluster proper motion rect ascension in degrees")
    ap.add_argument("-pmd", "--pmdec", required=False,
        help="Open cluster proper motion declination in degrees")
    ap.add_argument("-pmr", "--pm-radius", required=False,
        help="Open cluster proper motion radius in miliseconds of arc (mas)")
    ap.add_argument("-npmra", "--n-pmra", required=False,
        help="If pmra, pmd and pmr are not provided, we plot an histogram and fit to a gaussian. \
              The maximum of probability density function determines pmra and pmdec. \
              Then we calculate the full width (FWN) of the gaussian for 'n' percent of maximum (in pmra and pmdec). So \
              all sources inside FWN are over n%% of maximum of probability density. Default: 50%% (=FWHM). \
              FWN is used to calculate proper motion radii (A = pmra_FWN / 2 ; B = pmdec_FWN / 2). \
              Therefore, sources are members of open cluster if they are inside ellipse with semi-axis A and B.")
    ap.add_argument("-npmd", "--n-pmdec", required=False,
            help="Same as npmra, but for proper motion in declination. Default: 50%%. To calculate FWN_dec, and B.")   
    ap.add_argument("-bpmra", "--b-pmra", required=False,
        help="If pmra, pmd and pmr are not provided, we plot an histogram and fit to a gaussian. \
              The gaussian fit is performed through scikit KDE (Kernel Density Estimator). \
              'b' is the bandwidth of KDE fit, that acts as gaussian sharpener (lower b) or smoother (higher b). \
              Default: 1")
    ap.add_argument("-bpmd", "--b-pmdec", required=False,
            help="Same as bpmra, but for proper motion in declination. Default: 1")   
    ap.add_argument("-vp", "--visibility-periods", required=False,
            help="Required Gaia visibility periods to accept a source. Default: 6")
    ap.add_argument("-ruwe", "--ruwe", required=False,
            help="Rectified Unified Weight Error, Gaia goodness of fit. Default: 1.4")
    ap.add_argument("-pe", "--parallax_error", required=False,
            help="The maximum error on parallax for not discarding a source. Default: 0.1")
    ap.add_argument("-ns", "--n_sigma", required=False,
            help="Times parallax_error is allowed for selecting sources by group parallax. Default: 3")
    ap.add_argument("-pz", "--parallax_zero", required=False,
            help="The parallax zero point of Gaia, if not corrected distances are overestimated. Default: 0.040")
    ap.add_argument("-pez", "--parallax_error_zero", required=False,
            help="The parallax error zero point of Gaia, if not corrected parallax error is underestimated. Default: 0.010")
    ap.add_argument("-pme", "--pm_error", required=False,
            help="The maximum error on proper movement measurements to calculate pmra, pmd and pmr if were not given.\
                  Default: 0.1")
    ap.add_argument("-gmax", "--g-mag-maximum", required=False,
            help="The maximum Gaia magnitude (V), we consider only sources brighter than this.\
                  Default: 18")
    ap.add_argument("-np", "--no-plot", action='store_true',
            help="Do not plot anything")

    return ap

def parseArgs(ap):
    args = vars(ap.parse_args())
    return args
ap = buildParser()
args = parseArgs(ap)

# Arguments for Open Cluster NGC2264
#args['file'] = 'NGC2264.csv'
#args['ra'] = '100.25'
#args['dec'] = '9.75'
#args['radius'] = '1500' # arcs
#args['pmra'] = '-1.8' # marcs/a
#args['pmdec'] = '-3.7' # marcs/a
#args['pm_radius'] = '1.50' # marcs/a

# Error
if args['file'] and not os.path.isfile(args['file']):
    print("File not found!!")
    ap.print_help()
    sys.exit()

# Default arguments
if not args['output_file']:
    args['output_file'] = 'Gaia_sources.csv'
if not args['output_cluster_file']:
    args['output_cluster_file'] = 'OC_members.csv'
if not args['visibility_periods']:
    args['visibility_periods'] = '6'
if not args['ruwe']:
    args['ruwe'] = '1.4'
if not args['parallax_error']:
    args['parallax_error'] = '0.1'
if not args['n_sigma']:
    args['n_sigma'] = '3'
if not args['parallax_zero']:
    args['parallax_zero'] = '0.040'
if not args['parallax_error_zero']:
    args['parallax_error_zero'] = '0.010'
if not args['pm_error']:
    args['pm_error'] = '0.1'
if not args['n_pmra']:
    args['n_pmra'] = '50'
if not args['b_pmra']:
    args['b_pmra'] = '1'
if not args['n_pmdec']:
    args['n_pmdec'] = '50'
if not args['b_pmdec']:
    args['b_pmdec'] = '1'
if not args['g_mag_maximum']:
    args['g_mag_maximum'] = '18'

# Always filter sources with high flux excess
args['flux_excess'] = True

# Load cluster params
ra = float(args['ra'])
dec = float(args['dec'])
radius = float(args['radius']) / 3600 # Deg

# Download sources
if not args['file']:
    if args['field_size']:
        print("Downloading Gaia sources in field size")
        field_size = float(args['field_size'])
        coord = SkyCoord(ra=ra, dec=dec, unit=(u.degree, u.degree), frame='icrs')
        width = u.Quantity(field_size, u.arcsec)
        height = u.Quantity(field_size, u.arcsec)
        Gaia.ROW_LIMIT = -1
        r = Gaia.query_object_async(coordinate=coord, width=width, height=height)
        ascii.write(r, args['output_file'], format='csv', overwrite=True)
    else:
        print("Downloading Gaia sources in cone search with radius")
        coord = SkyCoord(ra=ra, dec=dec, unit=(u.degree, u.degree), frame='icrs')
        Gaia.ROW_LIMIT = -1
        rad = u.Quantity(radius, u.deg)
        r = Gaia.cone_search_async(coord, rad)
        r = r.get_results()
        ascii.write(r, args['output_file'], format='csv', overwrite=True)
    print("CSV file saved in " + args['output_file'] + ". Use it next time...")
    args['file'] = args['output_file']

def kde2D(x, y, bandwidth, xbins=1000j, ybins=1000j, **kwargs):
    """Build 2D kernel density estimate (KDE)."""

    # create grid of sample locations (default: 100x100)
    xx, yy = np.mgrid[x.min():x.max():xbins,
                      y.min():y.max():ybins]

    xy_sample = np.vstack([yy.ravel(), xx.ravel()]).T
    xy_train  = np.vstack([y, x]).T

    kde_skl = KernelDensity(bandwidth=bandwidth, **kwargs)
    kde_skl.fit(xy_train)

    # score_samples() returns the log-likelihood of the samples
    z = np.exp(kde_skl.score_samples(xy_sample))
    return xx, yy, np.reshape(z, xx.shape)

def estimate_pm(dataset):
    pmra = []
    pmdec = []
    for s in dataset:
        if float(s['pmra_error']) < float(args['pm_error']) and float(s['dec_error']) < float(args['pm_error']):
            pmra.append(float(s['pmra']))
            pmdec.append(float(s['pmdec']))

    estimations = {}
    fig, axes = plot.subplots(nrows=1, ncols=2, figsize=(15,20))
    for pm, name, x_axis, ax in zip([pmra, pmdec], ['pmra', 'pmdec'], [r'$\mu_\alpha$', r'$\mu_\delta$'], axes.flatten()):
        min_pm = int(min(pm)) - 1
        max_pm = int(max(pm)) + 1

        # Histogram
        bins = [i for i in range(min_pm, max_pm, 1)]
        ax.hist(x=pm, bins=bins, color='#F2AB6D', rwidth=0.9, density = True)
        ax.set_xlabel(x_axis + ' (mas/y)')
        ax.xaxis.set_minor_locator(mticker.AutoMinorLocator())
        ax.yaxis.set_minor_locator(mticker.AutoMinorLocator())
        ax.set_ylabel('Frequence (probability density)')

        # Gaussian fit
        n = float(args['n_' + name])
        b = float(args['b_' + name])

        # Gaussian KDE (Kernel density estimation)
        sample = np.array(pm)
        sample = sample.reshape((len(sample), 1))
        kde = KernelDensity(kernel='gaussian', bandwidth=b).fit(sample)
        sample_bins = np.linspace(min_pm, max_pm, 1000)
        sample_bins = np.array(sample_bins)
        sample_bins = sample_bins.reshape((len(sample_bins), 1))
        log_probabilities = kde.score_samples(sample_bins)
        probabilities = np.exp(log_probabilities)

        # Most probable value according to kernel fit
        density_maximum = max(probabilities)
        index_max = np.argmax(probabilities)
        most_probable_x = sample_bins[index_max]
        density_fwhm = density_maximum * 0.5
        d = density_maximum
        i = index_max
        while d > density_fwhm and i < max_pm:
            d = probabilities[i]
            i += 1
        fwhm_high = sample_bins[i]
        d = density_maximum
        i = index_max
        while d > density_fwhm and i > min_pm:
            d = probabilities[i]
            i -= 1
        fwhm_low = sample_bins[i]
        fwhm = fwhm_high - fwhm_low
        std_dev = fwhm/2.355

        if n != 50:
            density_n_percent = density_maximum * n / 100
            d = density_maximum
            i = index_max
            while d > density_n_percent and i < max_pm:
                d = probabilities[i]
                i += 1
            fwn_high = sample_bins[i]
            d = density_maximum
            i = index_max
            while d > density_n_percent and i > min_pm:
                d = probabilities[i]
                i -= 1
            fwn_low = sample_bins[i]
            fwn = fwn_high - fwn_low
        else:
            fwn = fwhm

        stats = "Max: " + "{:.2f}".format(most_probable_x[0]) + " mas/y"
        if n != 50:
            stats += ", FW " + str(n) + "% max: " + "{:.2f}".format(fwn[0]) + " mas/y"
        stats += r", FWHM: " + "{:.2f}".format(fwhm[0]) + ' mas/y, $\sigma$ = ' + "{:.2f}".format(std_dev[0]) + " mas/y"
        estimations[name] = most_probable_x[0]
        estimations[name + '_fwn'] = fwn[0]
        ax.set_title(name + ' histogram (bars of 1 mas/y) \n' + stats)
        ax.plot(sample_bins[:], probabilities)

    # Ellipse semi-axis
    estimations['A'] = estimations['pmra_fwn'] / 2
    estimations['B'] = estimations['pmdec_fwn'] / 2

    if not args['no_plot']:
        plot.show()

        # The same estimation using Kernel KDE 2D
        fig, ax = plot.subplots(nrows=1, ncols=1, figsize=(15,20))
        x = np.array(pmra)
        y = np.array(pmdec)
        b = min(float(args['b_pmra']), float(args['b_pmdec']))
        xx, yy, zz = kde2D(x, y, b)

        # Most probable value according to kernel 2D fit (if we want to update it, may differ a bit)
        #density_maximum = np.amax(zz)
        #coord_indexes = np.where(zz == density_maximum)
        #estimations['pmra'] = float(xx[coord_indexes])
        #estimations['pmdec'] = float(yy[coord_indexes])

        # Add ellipse
        e = Ellipse((estimations['pmra'], estimations['pmdec']), 2 * estimations['A'], 2 * estimations['B'], color='red', alpha=0.2)
        ax.set_aspect('equal')
        ax.set_xlabel(r'$\mu_\alpha$ (mas)')
        ax.set_ylabel(r'$\mu_\delta$ (mas)')
        ax.xaxis.set_minor_locator(mticker.AutoMinorLocator())
        ax.yaxis.set_minor_locator(mticker.AutoMinorLocator())

        ax.set_title('KDE 2D Probability Density Function')

        ax.pcolormesh(xx, yy, zz, shading='auto')
        ax.scatter(x, y, s=2, facecolor='white')
        ax.add_artist(e)

        plot.show()
    return estimations

# Calulate weighted parallax of group
def calc_weighted_grp_plx(dataset):
    weights = []
    weights_sum = 0
    for sample in dataset:
        weights_sum += 1/(sample['parallax_error'] ** 2)
    weights = [ ( 1/(sample['parallax_error'] ** 2) ) / weights_sum for sample in dataset]
    parallax_g = 0
    parallax_g_squared_err = 0
    for w, sample in zip(weights, dataset):
        term = w * float(sample['parallax'])
        parallax_g += term
        err_term = (w ** 2) * (float(sample['parallax_error']) ** 2)
        parallax_g_squared_err += err_term

    # Add spatial covariance term to error
    V1 = load_V1_table_file()
    covariance_sum = 0
    i = 0
    while i < len(dataset) - 1:
        cov_sample_sum = 0
        j = 0
        while j < len(dataset):
            cov_sample_sum += weights[i] * weights[j] * get_spatial_covariance(float(dataset[i]['parallax']) - float(dataset[j]['parallax']), V1)
            j += 1
        covariance_sum += cov_sample_sum
        i += 1

    # Calculate parallax_g_err
    parallax_g_squared_err += 2 * covariance_sum
    parallax_g_err = math.sqrt(parallax_g_squared_err)

    return parallax_g, parallax_g_err

# Calulate weighted proper motion of group
def calc_weighted_grp_proper_motion(dataset):
    pm_output = {}
    for pm in ['pmra', 'pmdec']:
        weights = []
        weights_sum = 0
        for sample in dataset:
            weights_sum += 1/(sample[pm + '_error'] ** 2)
        weights = [ ( 1/(sample[pm + '_error'] ** 2) ) / weights_sum for sample in dataset]
        pm_g = 0
        pm_g_squared_err = 0
        for w, sample in zip(weights, dataset):
            term = w * float(sample[pm])
            pm_g += term
            err_term = (w ** 2) * (float(sample[pm + '_error']) ** 2)
            pm_g_squared_err += err_term

        # Add spatial covariance term to error
        V1 = load_V1_table_file()
        covariance_sum = 0
        i = 0
        while i < len(dataset) - 1:
            cov_sample_sum = 0
            j = i + 1 
            while j < len(dataset):
                cov_sample_sum += weights[i] * weights[j] * get_pm_spatial_covariance(float(dataset[i]['parallax']) - float(dataset[j]['parallax']), V1)
                j += 1
            covariance_sum += cov_sample_sum
            i += 1

        # Calculate proper motion error
        pm_g_squared_err += 2 * covariance_sum
        pm_g_err = math.sqrt(pm_g_squared_err)
        pm_output[pm] = pm_g
        pm_output[pm + '_error'] = pm_g_err

    return pm_output

# Start working...
if args['file']:
    print("Processing file: " + args['file'])
    print("=================================\n")
    dataset = []
    with open(args['file'],'r') as fp:
        for sample in csv.DictReader(fp):
            dataset.append(sample)
    csv_columns = list(dataset[0].keys())
    print("Dataset loaded: " + str(len(dataset)) + ' stars')

    # Get stars with five parameter astrometry
    dataset_five_param = []
    for sample in dataset:
        skip = False
        for k in five_param_astrometry:
            if sample[k] == '':
                skip = True
                break
        if not skip:
            dataset_five_param.append(sample)
    print("Five parameters astrometry of: " + str(len(dataset_five_param)))
    dataset = dataset_five_param

    # Get stars with more than n visibility_periods
    if args['visibility_periods']:
        dataset_visibility = []
        for sample in dataset:
            if int(sample['visibility_periods_used']) >= int(args['visibility_periods']):
                dataset_visibility.append(sample)
        dataset = dataset_visibility

    # Get stars with consistent flux excess
    if args['flux_excess']:
        dataset_fe = []
        for sample in dataset:
            x = 1.3 + 0.06 * (float(sample['bp_rp'])) ** 2
            if float(sample['phot_bp_rp_excess_factor']) <= x:
                dataset_fe.append(sample)
        print("Flux excess: C < 1.3 + 0.06 (Gbp - Grp)^2: " + str(len(dataset_fe)))
        dataset = dataset_fe

    # Corrections according to Gaia recomendations
    dataset_corrected = []
    for sample in dataset:
        # Correct G
        g = float(sample['phot_g_mean_mag'])
        if g < 6:
            g_corr = g - 0.0032 * (g - 6)
        else:
            g_corr = g + 0.0271 * (6 - g)
        sample['phot_g_mean_mag'] = g = g_corr

        # Correct std dev in position
        ra_error = float(sample['ra_error'])
        dec_error = float(sample['dec_error'])
        if g < 13:
            ra_std_dev = 1.08 ** 2 * ra_error ** 2 + 0.016 ** 2
            dec_std_dev = 1.08 ** 2 * dec_error ** 2 + 0.016 ** 2
        else:
            ra_std_dev = 1.08 ** 2 * ra_error ** 2 + 0.033 ** 2
            dec_std_dev = 1.08 ** 2 * dec_error ** 2 + 0.033 ** 2
        sample['ra_error'] = ra_error = math.sqrt(ra_std_dev)
        sample['dec_error'] = dec_error = math.sqrt(dec_std_dev)

        # Correct std dev in parallax
        p_error = float(sample['parallax_error'])
        if g < 13:
            std_dev = 1.08 ** 2 * p_error ** 2 + 0.021 ** 2
        else:
            std_dev = 1.08 ** 2 * p_error ** 2 + 0.043 ** 2
        sample['parallax_error'] = p_error = math.sqrt(std_dev)

        # Correct std dev in proper motion
        pmra_error = float(sample['pmra_error'])
        pmdec_error = float(sample['pmdec_error'])
        if g < 13:
            pmra_std_dev = 1.08 ** 2 * pmra_error ** 2 + 0.032 ** 2
            pmdec_std_dev = 1.08 ** 2 * pmdec_error ** 2 + 0.032 ** 2
        else:
            pmra_std_dev = 1.08 ** 2 * pmra_error ** 2 + 0.066 ** 2
            pmdec_std_dev = 1.08 ** 2 * pmdec_error ** 2 + 0.066 ** 2
        sample['pmra_error'] = pmra_error = math.sqrt(pmra_std_dev)
        sample['pmdec_error'] = pmdec_error = math.sqrt(pmdec_std_dev)

        if g < float(args['g_mag_maximum']) and p_error < float(args['parallax_error']):
        #if g < float(args['g_mag_maximum']):
            dataset_corrected.append(sample)
    print("Parallax error < " + args['parallax_error'] + ": " + str(len(dataset_corrected)))
    dataset = dataset_corrected

    # Cluster center
    # Center in position
    dataset_center = []
    for sample in dataset:
        ra_diff = float(sample['ra']) - ra
        dec_diff = float(sample['dec']) - dec
        pos = math.sqrt(ra_diff ** 2 + dec_diff ** 2)
        if pos <= radius:
            sample['radius'] = pos
            dataset_center.append(sample)
    dataset = dataset_center
    msg = "Centered in pos: ra " + args['ra'] + ", dec " + args['dec'] + ", radius " + args['radius'] 
    msg += ": " + str(len(dataset_center))
    print(msg)

    # Calculate RUWE. At the end, more CPU consuming
    if args['ruwe']:
        dataset_ruwe = []
        U0 = load_U0_table_file()
        for sample in dataset:
            # Calculate UWE
            uwe = math.sqrt(float(sample['astrometric_chi2_al']) / (int(sample['astrometric_n_good_obs_al']) - 5) )
            g_mag = round(float(sample['phot_g_mean_mag']),2)
            bp_rp = round(float(sample['bp_rp']),1)
            # Get u0
            ii = np.where(U0 == g_mag)[0]
            jj = np.where(U0 == bp_rp)[0]
            x = np.intersect1d(ii,jj)[0]
            u0 = U0[x, 2]
            # Calculate RUWE
            ruwe = uwe / u0
            if ruwe <= float(args['ruwe']):
                dataset_ruwe.append(sample)
        print("RUWE < " + args['ruwe'] + ": N(*,0) = " + str(len(dataset_ruwe)))
        dataset = dataset_ruwe

    # If we do not have cluster proper motion params, estimate them
    if not (args['pmra'] and args['pmdec'] and args['pm_radius']):
        params = estimate_pm(dataset)
    else:
        params = {}
        params['pmra'] = float(args['pmra'])
        params['pmdec'] = float(args['pmdec'])
        params['A'] = float(args['pm_radius'])
        params['B'] = float(args['pm_radius'])

    # Center in proper movement
    dataset_center_pm = []
    for sample in dataset:
        pmra_diff = float(sample['pmra']) - params['pmra']
        pmdec_diff = float(sample['pmdec']) - params['pmdec']
        pm = math.sqrt( (pmra_diff/params['A']) ** 2 + (pmdec_diff/params['B']) ** 2)
        # If the source is inside ellipse semi-axis A and B.
        # If sqrt ( pmra_diff^2/A + pmdec_diff^2/B ) <= 1.
        if pm <= 1:
            dataset_center_pm.append(sample)
    msg = "Centered in proper motion: pmra " + "{:.2f}".format(params['pmra']) + ", pmdec " + "{:.2f}".format(params['pmdec'])
    if params['A'] == params['B']:
        msg += ", radius " + "{:.2f}".format(params['A'])
    else:
        msg += ", Ellipse A = " + "{:.2f}".format(params['A']) + ", B = " + "{:.2f}".format(params['B'])
    msg += ": " + str(len(dataset_center_pm))
    print(msg)
    dataset = dataset_center_pm

    if not args['no_plot']:
        # 2 plots, position and proper motion
        # Plot samples before centering in position (dataset_corrected)
        fig, axes = plot.subplots(nrows=1, ncols=2, figsize=(15,20))
        ax = axes.flatten()[0]
        ax.set_aspect('equal')
        title = 'Declination vs Rect ascension \n'
        title += r'Center in $\alpha$ = ' + args['ra'] + " deg, $\delta$ = " + args['dec'] + " deg, r = " + args['radius'] + ' arcsec'
        ax.set_title(title)
        ax.set_ylabel(r'$\delta$ (deg)')
        ax.set_xlabel(r'$\alpha$ (deg)')
        ax.yaxis.set_minor_locator(mticker.AutoMinorLocator())
        ax.xaxis.set_minor_locator(mticker.AutoMinorLocator())
        x = [float(s['ra']) for s in dataset_corrected]
        y = [float(s['dec']) for s in dataset_corrected]
        e = Ellipse((ra, dec), 2 * radius, 2 * radius, color='red', alpha=0.2)
        x_in = [float(s['ra']) for s in dataset]
        y_in = [float(s['dec']) for s in dataset]

        ax.add_artist(e)
        ax.scatter(x, y, c='red', s=1)
        ax.scatter(x_in, y_in, marker='X', c='blue', s=6)

        # Plot samples before filter, for pmra and pmdec (dataset_center)
        ax = axes.flatten()[1]
        ax.set_aspect('equal')
        title = 'Proper motion Declination vs Rect ascension \n'
        title += r'Center in $\mu_\alpha$ = ' + "{:.2f}".format(params['pmra']) + " mas/y, $\mu_\delta$ = " + "{:.2f}".format(params['pmdec']) + " mas/y\n"
        title += r'Ellipse: $A_{\mu_\alpha}$ = ' + "{:.2f}".format(params['A']) + 'mas/y, $B_{\mu_\delta}$ = ' + "{:.2f}".format(params['B']) +  " mas/y"
        ax.set_title(title)
        ax.set_ylabel(r'$\mu_\delta$ (mas/y)')
        ax.set_xlabel(r'$\mu_\alpha$ (mas/y)')
        ax.yaxis.set_minor_locator(mticker.AutoMinorLocator())
        ax.xaxis.set_minor_locator(mticker.AutoMinorLocator())
        x = [float(s['pmra']) for s in dataset_center]
        y = [float(s['pmdec']) for s in dataset_center]
        e = Ellipse((params['pmra'], params['pmdec']), 2 * params['A'], 2 * params['B'], color='red', alpha=0.2)
        x_in = [float(s['pmra']) for s in dataset]
        y_in = [float(s['pmdec']) for s in dataset]

        ax.add_artist(e)
        ax.scatter(x, y, c='red', s=1)
        ax.scatter(x_in, y_in, marker='X', c='blue', s=6)
        plot.show()

# Calulate weighted parallax of group
parallax_g_zero, parallax_g_zero_err = calc_weighted_grp_plx(dataset)

# Discard sources out of 3 * std_dev
dataset_plx_corrected = []
parallax_g_zero_err_max = float(args['n_sigma']) * float(args['parallax_error'])
#parallax_g_zero_err_max = float(args['n_sigma']) * parallax_g_zero_err
parallax_up = parallax_g_zero + parallax_g_zero_err_max
parallax_down = parallax_g_zero - parallax_g_zero_err_max

for sample in dataset:
    if parallax_down <= float(sample['parallax']) and float(sample['parallax']) <= parallax_up: 
        dataset_plx_corrected.append(sample)
dataset = dataset_plx_corrected
N = str(len(dataset_plx_corrected))
print("Parallax deviates less than " + args['n_sigma'] + "-sigma from group parallax: N(*) = " + N)

# Save members
with open(args['output_cluster_file'], 'w') as csvfile:
    writer = csv.DictWriter(csvfile, fieldnames=dataset[0].keys())
    writer.writeheader()
    for item in dataset:
        writer.writerow(item)

# Re-calculate group parallax for final OC members
parallax_g, parallax_g_err = calc_weighted_grp_plx(dataset)

# Correct parallax of group by bias, parallax zero point:
parallax_g += float(args['parallax_zero'])
parallax_g_err = math.sqrt(parallax_g_err ** 2 + float(args['parallax_error_zero']) ** 2)

# Calculate center and proper motion average and error
print("\nStats of samples in N(*)")
print("========================\n")
stats = {}
keys = ['ra','dec', 'pmra','pmdec', 'parallax']
units = ['deg', 'deg', 'mas/yr', 'mas/yr', 'mas']

for k in keys:
    stats[k] = sum(float(s[k]) for s in dataset) / len(dataset) 
    squared_error = sum((float(s[k]) - stats[k]) ** 2 for s in dataset)
    stats[k + '_error']  = math.sqrt( 1/len(dataset) * squared_error )

for k,u in zip(keys,units):
    my_error = "{:.2f}".format(stats[k + '_error'])
    print(k + ': ' + "{:.2f}".format(stats[k]) + u"\u00B1" + my_error + ' ' + u)

y = [float(s['parallax']) for s in dataset]
yerr = [float(s['parallax_error']) for s in dataset]

max_parallax = max(y)
min_parallax = min(y)

# Distance using statistical data
distance = 1/stats['parallax'] * 1000
distance_up = 1/(stats['parallax'] - stats['parallax_error']) *1000
error_up = distance_up - distance
distance_down = 1/(stats['parallax'] + stats['parallax_error']) *1000
error_down = distance - distance_down
print('Distance (mean and error): ' + "{:.0f}".format(distance) + "(+" + "{:.0f}".format(error_up) + '/-' + "{:.0f}".format(error_down) + ") pc")

print("\nPrecision astrometry for group N(*)")
print("===================================\n")
print('Parallax group: ' + "{:.3f}".format(parallax_g) + u"\u00B1" + "{:.3f}".format(parallax_g_err) + ' mas')
# Distance using parallax group
distance = 1 / parallax_g * 1000
distance_up = 1/(parallax_g - parallax_g_err) *1000
error_up = distance_up - distance
distance_down = 1/(parallax_g + parallax_g_err) *1000
error_down = distance - distance_down
print('Distance for parallax group (with error): ' + "{:.0f}".format(distance) + "(+" + "{:.0f}".format(error_up) + '/-' + "{:.0f}".format(error_down) + ") pc")

# Distance max and min 
distance_max = 1 / min_parallax * 1000
distance_min = 1 / max_parallax * 1000
print("Max and min distances in group: (d_max: " + "{:.0f}".format(distance_max) + ' / d_min: ' + "{:.0f}".format(distance_min) + ") pc")

# r50
oc_all_radius = [sample['radius'] for sample in dataset]
oc_all_radius = sorted(oc_all_radius)
r50deg = oc_all_radius[round(len(oc_all_radius)/2)-1]
r50pc = r50deg * math.pi / 180 * distance
print("R50 : " + "{:.3f}".format(r50deg) + ' deg, ' + "{:.3f}".format(r50pc) + ' pc')

# Calulate weighted proper motion of group
pm = calc_weighted_grp_proper_motion(dataset)
u = 'mas/y'
for k in ['pmra','pmdec']:
    print(k + ': ' + "{:.3f}".format(pm[k]) + u"\u00B1" + "{:.3f}".format(pm[k + '_error']) + ' ' + u)

if args['name']:
    cluster = {}
    cluster['Cluster'] = args['name']
    cluster['in_ra'] = args['ra']
    cluster['in_dec'] = args['dec']
    cluster['in_radius'] = args['radius']
    if not (args['pmra'] and args['pmdec'] and args['pm_radius']):
        cluster['in_pmra'] = '-'
        cluster['in_pmdec'] = '-'
        cluster['in_pm_radius'] = '-'
    else:
        cluster['in_pmra'] = args['pmra']
        cluster['in_pmdec'] = args['pmdec']
        cluster['in_pm_radius'] = args['pm_radius']
    cluster['N'] = N
    for k in ['pmra','pmdec']:
        cluster[k] = "{:.3f}".format(pm[k])
        cluster[k + '_error'] = "{:.3f}".format(pm[k + '_error'])
    cluster['parallax'] = "{:.3f}".format(parallax_g)
    cluster['parallax_error'] = "{:.3f}".format(parallax_g_err)
    cluster['r50'] = "{:.3f}".format(r50deg)
    cluster['distance'] = "{:.0f}".format(distance)
    cluster['distance_up'] = "{:.0f}".format(error_up)
    cluster['distance_down'] = "{:.0f}".format(error_down)

    print("Openning clusters file...")
    cluster_dataset = []
    with open(my_cluster_file,'r') as fp:
        for sample in csv.DictReader(fp):
            cluster_dataset.append(sample)
    csv_columns = cluster.keys()
    print("Cluster dataset loaded: " + str(len(cluster_dataset)) + ' clusters')

    print("Adding or editting cluster...")
    cluster_names = [sample['Cluster'] for sample in cluster_dataset]
    if cluster['Cluster'] in cluster_names:
        index = cluster_names.index(cluster['Cluster'])
        cluster_dataset[index] = cluster
    else:
        cluster_dataset.append(cluster)
    # Save clusters
    with open(my_cluster_file, 'w') as csvfile:
        writer = csv.DictWriter(csvfile, fieldnames=csv_columns)
        writer.writeheader()
        for item in cluster_dataset:
            writer.writerow(item)
    print("OK!")

if not args['no_plot']:
    # 2 plots 
    # Plot parallax errors vs Gbp-Grp
    fig, axes = plot.subplots(nrows=1, ncols=2, figsize=(15,20))
    ax = axes.flatten()[0]
    title = r'Parallax vs color $G_{bp} - G_{rp}$'
    ax.set_title(title)
    ax.set_ylabel(r'$\omega$ (mas)')
    ax.set_xlabel(r'$G_{bp} - G_{rp}$ (magnitud)')
    ax.yaxis.set_minor_locator(mticker.AutoMinorLocator())
    ax.xaxis.set_minor_locator(mticker.AutoMinorLocator())

    x1 = [float(s['bp_rp']) for s in dataset_center_pm]
    x2 = [float(s['bp_rp']) for s in dataset]
    y_center_pm = [float(s['parallax']) for s in dataset_center_pm]
    yerr_center_pm = [float(s['parallax_error']) for s in dataset_center_pm]
    # Same as above for dataset (selected samples of cluster)
    #y = [float(s['parallax']) for s in dataset]
    #yerr = [float(s['parallax_error']) for s in dataset]

    ax.errorbar(x1, y_center_pm, yerr=yerr_center_pm, fmt='o', color='lightgray', 
                 ecolor='lightgray', elinewidth=1, capsize=3, linestyle="None")
    ax.errorbar(x2, y, yerr=yerr, fmt='o', color='blue', 
                 ecolor='blue', elinewidth=1, capsize=3, linestyle="None")
    ax.axhline(y=parallax_g, color='g', linestyle='-')
    ax.axhline(y=parallax_g_zero, color='g', linestyle='--')

    # Plot parallax errors vs G'
    ax = axes.flatten()[1]
    title = "Parallax vs G' magnitude"
    ax.set_title(title)
    ax.set_ylabel(r'$\omega$ (mas)')
    ax.set_xlabel("G' (magnitud)")
    ax.yaxis.set_minor_locator(mticker.AutoMinorLocator())
    ax.xaxis.set_minor_locator(mticker.AutoMinorLocator())

    x1 = [float(s['phot_g_mean_mag']) for s in dataset_center_pm]
    x2 = [float(s['phot_g_mean_mag']) for s in dataset]
    # Same as above for y_center_pm, y

    ax.errorbar(x1, y_center_pm, yerr=yerr_center_pm, fmt='o', color='lightgray',
                 ecolor='lightgray', elinewidth=1, capsize=3, linestyle="None")
    ax.errorbar(x2, y, yerr=yerr, fmt='o', color='blue', 
                 ecolor='blue', elinewidth=1, capsize=3, linestyle="None")
    ax.axhline(y=parallax_g, color='g', linestyle='-')
    ax.axhline(y=parallax_g_zero, color='g', linestyle='--')
    plot.show()

    # 2 plots 
    # Plot parallax histograms
    fig, axes = plot.subplots(nrows=1, ncols=2, figsize=(15,20))
    ax = axes.flatten()[0]
    title = r'Histogram of parallaxes'
    ax.set_title(title)
    ax.set_xlabel(r'$\omega$ (mas)')
    ax.set_ylabel("N")

    # Histogram
    step = 0.05
    a = [float(sample['parallax']) for sample in dataset_five_param]
    b = [float(sample['parallax']) for sample in dataset_center_pm]
    c = [float(sample['parallax']) for sample in dataset]
    #N = int((max(a) - min(a))/step)
    #bins = np.linspace(min(a), min(a) + (step*N), N)
    start = 0
    end = round(parallax_g) + 1
    N = int(end/step)
    bins = np.linspace(start, end, N)
    ax.hist(x=a, bins=bins, rwidth=1, color='black', fill=False, histtype='step', stacked=True, label="All Gaia stars")
    ax.hist(x=b, bins=bins, rwidth=1, color='red', fill=False, histtype='step', stacked=True, label="Stars before parallax group filter")
    ax.hist(x=c, bins=bins, rwidth=1, color='blue', fill=False, histtype='step', stacked=True, label="OC members")
    ax.set_yscale('log')
    ax.yaxis.set_major_formatter(mticker.ScalarFormatter())
    ax.xaxis.set_minor_locator(mticker.AutoMinorLocator())
    ax.set_xlim(start + step, end - step)
    ax.axvline(x=parallax_g, color='g', linestyle='-')
    ax.axvline(x=parallax_g_zero, color='g', linestyle='--')
    ax.legend(loc="upper right")

    # Histogram of deviation from group parallax
    ax = axes.flatten()[1]
    title = r'Histogram of parallax deviation from group parallax'
    ax.set_title(title)
    ax.set_xlabel(r'$(\omega - \omega_g)/\sigma_\omega$ (mas)')
    ax.set_ylabel("N")
    a = [(float(sample['parallax'])-parallax_g) / float(sample['parallax_error']) for sample in dataset_five_param]
    b = [(float(sample['parallax'])-parallax_g) / float(sample['parallax_error']) for sample in dataset_center_pm]
    c = [(float(sample['parallax'])-parallax_g) / float(sample['parallax_error']) for sample in dataset]
    step = 1
    start = -11
    end = 11
    bins = np.linspace(start, end, N)
    ax.hist(x=a, bins=bins, rwidth=1, color='black', fill=False, histtype='step', stacked=True, label="All Gaia stars")
    ax.hist(x=b, bins=bins, rwidth=1, color='red', fill=False, histtype='step', stacked=True, label="Stars before parallax group filter")
    ax.hist(x=c, bins=bins, rwidth=1, color='blue', fill=False, histtype='step', stacked=True, label="OC members")
    ax.set_yscale('log')
    ax.yaxis.set_major_formatter(mticker.ScalarFormatter())
    ax.xaxis.set_minor_locator(mticker.AutoMinorLocator())
    ax.set_xlim(start + step, end - step)
    ax.legend(loc="upper right")

    plot.show()

