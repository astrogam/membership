import astropy.units as u
from astropy.coordinates import SkyCoord
from astroquery.gaia import Gaia
from astropy.io import ascii

"""
coord = SkyCoord(ra=280, dec=-60, unit=(u.degree, u.degree), frame='icrs')
width = u.Quantity(0.1, u.deg)
height = u.Quantity(0.1, u.deg)
r = Gaia.query_object_async(coordinate=coord, width=width, height=height)
ascii.write(r, 'values.dat', format='csv', overwrite=True)
r.pprint()
"""
#job = Gaia.launch_job("select top 100 "
#                      "solution_id,ref_epoch,ra_dec_corr,astrometric_n_obs_al, "
#                      "matched_observations,duplicated_source,phot_variable_flag "
#                      "from gaiadr2.gaia_source order by source_id")
#r = job.get_results()
#print(r['solution_id'])

#tables = Gaia.load_tables(only_names=True)

query = ("select * from gaiadr2.tmass_best_neighbour where source_id=1635721458409799680")
job = Gaia.launch_job(query=query)
results = job.get_results()

print(f"results = {results}")

