import matplotlib.pyplot as plot        
import csv
import matplotlib.markers as markers
import matplotlib.ticker as mticker
from matplotlib.colors import ListedColormap, rgb2hex
import matplotlib.patches as mpatches

dataset = []
preds = []
members_f = 'OC_members/M67.csv'
cluster = members_f.split('/')[1]
predictions_f = '../StarClasif/predictions_Vizier_to_bvjhk.csv'
predictions_f = '../StarClasif/predictions.csv'

title = "HR diagrams of OC " + cluster.replace('_', ' ').replace('.csv','')

classes = {"M": 1, "K": 2, "G": 3, "F": 4, "A": 5, "B": 6}
with open(members_f,'r') as fp:
    for sample in csv.DictReader(fp):
        dataset.append(sample)

with open(predictions_f,'r') as fp:
    for sample in csv.DictReader(fp):
        if sample['Cluster'] == cluster and sample["Linear SVM"]:
            sample['CLASS'] = classes[sample["Linear SVM"]]
            preds.append(sample)

step = 1/(len(classes))
yticks = [0 + step/2]
for i in range(len(classes)-1):
    yticks.append(yticks[-1]+step)

# Get the color map, reversed (Blue=Hot)
cmap = plot.cm.coolwarm.reversed()
my_colors = []
for i in yticks:
    rgba = cmap(round(i*cmap.N))
    my_colors.append(rgb2hex(rgba))
cmap = ListedColormap(my_colors)

fig, axes = plot.subplots(nrows=1, ncols=2, figsize=(15,20))
fig.suptitle(title)

# HR G vs Bp-Rp
ax = axes.flatten()[0]
ax.set_box_aspect(1)
title = r'G vs $\mathregular{G_{bp}-G_{rp}}$'
ax.set_title(title)
ax.set_ylabel(r'G')
ax.set_xlabel(r'$\mathregular{G_{bp}-G_{rp}}$')
ax.yaxis.set_minor_locator(mticker.AutoMinorLocator())
ax.xaxis.set_minor_locator(mticker.AutoMinorLocator())
ax.set_xlim([0,5])
ax.invert_yaxis()
x = [float(s['bp_rp']) for s in dataset]
y = [float(s['phot_g_mean_mag']) for s in dataset]
ax.scatter(x, y, c='grey', s=10)

x_pred = [float(s['bp_rp']) for s in preds]
y_pred = [float(s['phot_g_mean_mag']) for s in preds]
c_pred = [s['CLASS'] for s in preds]
ax.scatter(x_pred, y_pred, c=c_pred, cmap=cmap, s=20)

# HR G vs Teff
ax = axes.flatten()[1]
ax.set_box_aspect(1)
title = r'G vs $\mathregular{t_{eff}}$'
ax.set_title(title)
ax.set_ylabel(r'G')
ax.set_xlabel(r'$\mathregular{t_{eff}}$')
ax.yaxis.set_minor_locator(mticker.AutoMinorLocator())
ax.xaxis.set_minor_locator(mticker.AutoMinorLocator())
ax.invert_yaxis()
ax.set_xlim([3000,10000])
ax.invert_xaxis()
x = []
y = []
for s in dataset:
    if s['teff_val'] != '':
        x.append(float(s['teff_val'])) 
        y.append(float(s['phot_g_mean_mag']))
ax.scatter(x, y, c='grey', s=10)

x_pred = []
y_pred = []
c_pred = []
for s in preds:
    if s['teff_val'] != '':
        x_pred.append(float(s['teff_val'])) 
        y_pred.append(float(s['phot_g_mean_mag']))
        c_pred.append(s['CLASS'])
        #x_pred = [float(s['teff_val']) for s in preds]
        #y_pred = [float(s['phot_g_mean_mag']) for s in preds]
        #c_pred = [s['CLASS'] for s in preds]
ax.scatter(x_pred, y_pred, c=c_pred, cmap=cmap, s=20)

for ax in axes.flatten():
    my_mpatches = []
    for color, classe in zip(my_colors, classes):
        my_mpatches.append(mpatches.Patch(color=color, label=classe))
    my_mpatches.reverse()
    ax.legend(handles=my_mpatches, loc=2)

plot.show()

