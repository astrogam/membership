Gaia DR2: Spatial covariance of parallax and proper motion errors (from QSOs)

L. Lindegren (V1, 2018 Aug 10)

The file spatialCovarianceQso.txt contains the functions V_plx(theta) and
V_mu(theta) shown in Figs. 14 and 15 of Lindegren et al. A&A 616, A2 (2018)
(https://doi.org/10.1051/0004-6361/201832727). The data correspond to the
red dots in the figures, i.e. the raw estimates of the covariances. The
uncertainties (red error bars) and smoothed values (blue curves) are not
given in the data file.

spatialCovarianceQso.txt (65889 bytes) has 3 columns (theta,V_plx,V_mu)
and 1440 data lines (+ header) for theta = 0.0625(0.125)179.9375.

Column descriptions:
theta = angle, unit: [deg]
V_plx = covariance of parallax errors for QSOs separated by the angle theta,
unit: [microarcsec^2]
V_mu = covariance of proper motion errors (per component) for QSOs separated 
by the angle theta, unit: [(microarcsec/yr)^2]

The angle theta is the centre of the bin, i.e. the first bin corresponds to
the interval theta = 0 to 0.125 deg.

See Lindegren et al. A&A 616, A2 (2018), Sect. 5.4 for details.
